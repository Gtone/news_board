# News board
Сайт вылит на zzz.com.ua:
http://feedback-board.zzz.com.ua

Проект реализован по паттерну MVC, без использования фреймворка.
Верстка - бустрап.

## Плагины
  - [jQuery](https://jquery.org/license/) 
  - [Image preview](https://github.com/zpalffy/preview-image-jquery) 
  - [Read more](https://github.com/jedfoster/Readmore.js) 
  - [CAPTCHA](https://github.com/yasirmturk/simple-php-captcha) 
### Hints
  - Routes config: www/config/routes.php
  - Admin actions permission config: www/config/permissions.php
  - Database config: www/config/db_params.php
  - Site database SQL dump: sql/DB_MAIN_DUMP.sql

.htaccess перенаправляет все файлы на index.php - Front Controller

Модули разбиты на вкладки в шапке сайта. В админке некоторые действия соеденены в одну табличку, для удобства.
Создание отзывов и создание новостей вынесены на отдельные страницы.


