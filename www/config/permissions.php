<?php
//Admin actions permissions config
    return [
        'CabinetController/actionIndex' => true,
        'CabinetController/actionNews' => true,
        'CabinetController/actionNewsShow' => true,
        'CabinetController/actionNewsCreate' => true,
        'CabinetController/actionNewsDelete' => true,
        
        'CabinetController/actionReviews' => true,
        'CabinetController/actionReviewsDelete' => true,
        'CabinetController/actionReviewsEdit' => true,
        'CabinetController/actionReviewsShow' => true,
    ];