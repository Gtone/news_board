<?php
//All implemented site actions config
    return [
        'news/([0-9]+)' => 'news/view/$1',
        'news/page-([0-9]+)' => 'news/index/$1',
        'news' => 'news/index',
        
        'reviews/page-([0-9]+)' => 'reviews/index/$1',
        'reviews/add' => 'reviews/add',
        'reviews' => 'reviews/index',
        
        'admin' => 'admin/login',
        'admin/logout' => 'admin/logout',
        
        'cabinet' => 'cabinet/index',
        'cabinet/news' => 'cabinet/news',
        'cabinet/news/list' => 'cabinet/newsShow',
        'cabinet/news/create' => 'cabinet/newsCreate',
        'cabinet/news/delete/([0-9]+)' => 'cabinet/newsDelete/$1',
        
        'cabinet/reviews' => 'cabinet/reviews',
        'cabinet/reviews/list' => 'cabinet/reviewsShow',
        'cabinet/reviews/delete/([0-9]+)' => 'cabinet/reviewsDelete/$1',
        'cabinet/reviews/edit/([0-9]+)' => 'cabinet/reviewsEdit/$1',
        
        '' => 'site/index',
    ];