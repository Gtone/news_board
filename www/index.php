<?php   
//FRONT CONTROLLER
//print $_SERVER['DOCUMENT_ROOT'];
    //1. Common settings
    error_reporting(E_ALL);
    ini_set('display_errors', 1);
    
    session_start();
    
    //2. System file includes
    define('ROOT', dirname(__FILE__));
    require_once (ROOT . '/vendor/Simple_php_captcha.php');
    require_once (ROOT . '/components/Autoload.php');
    
    //4. Exec router
    $router = new Router();
    $router->run();