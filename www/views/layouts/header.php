<!doctype html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <title>Board</title>
        <!---->
        <link href="/template/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
        <link href="/template/css/bootstrap-theme.min.css" rel="stylesheet" type="text/css"/>
        <link href="/template/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
         <!--Latest compiled and minified CSS 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u" crossorigin="anonymous">-->

         <!--Optional theme 
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap-theme.min.css" integrity="sha384-rHyoN1iRsVXV4nD0JutlnGaslCJuC7uwjduW9SVrLvRYooPp2bWYgmgJQIXwl/Sp" crossorigin="anonymous">-->
    
         <link rel="stylesheet" href="/template/css/custom.css">
    </head>
    <body>
        <div class="navbar navbar-inverse navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">
                    <button class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>
                    <a href="/" class="navbar-brand">newsBoard</a>
                </div>
                <ul class="nav navbar-nav collapse navbar-collapse">
                    <li <?php if(CONTROLLER_NAME == 'NewsController'):?>class="active"<?php endif;?>><a href="/news/">Новости</a></li>
                    <li <?php if(CONTROLLER_NAME == 'ReviewsController'):?>class="active"<?php endif;?>><a href="/reviews/">Отзывы</a></li>
                </ul>
                <ul class="nav navbar-nav collapse navbar-collapse pull-right">
                    <?php if (Admin::isGuest()):?>
                        <li><a href="/admin/"><i class="fa fa-lock"></i> Вход</a></li>
                    <?php else: ?>
                        <li <?php if(CONTROLLER_NAME == 'CabinetController'):?>class="active"<?php endif;?>><a href="/cabinet/">Панель</a></li>
                        <?php $adminData = Admin::getAdminById($_SESSION["admin"]); ?>
                        <li><span id="admin" class="label label-default">Welcome, <?=$adminData["login"]?></span></li>
                        <li><a href="/admin/logout/"><i class="fa fa-unlock"></i> Выход</a></li>
                    <?php endif; ?>
                </ul>
            </div>
        </div>
        <div class="container" id="main">