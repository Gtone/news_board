<?php include (ROOT . '/views/layouts/header.php'); ?>

<form class="" method="post">
    <div class="row">
        <div class="col-md-6 col-md-offset-3">
            <h2>Admin Login</h2>
            <?php if(isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?=$error;?></li>
                        <?php endforeach; ?>
                    </ul>
            <?php endif; ?>
            <div class="form-group">
                <label for="login">Login:</label>
                <input type="text" placeholder="login" class="form-control" name="login" required="">
            </div>
            <div class="form-group">
                <label for="password">Password:</label>
                <input type="password" placeholder="password" class="form-control" name="password" required="">
            </div>   
            <input type="submit" name="submit" class="btn btn-success">   
        </div>
    </div>
</form>

