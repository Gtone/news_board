<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet/">Admin panel</a></li>
                    <li><a href="/cabinet/news">News management</a></li>
                    <li class="active">List of news</li>
                </ol>
            </div>
            <h3>The list of news:</h3>
            
            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Title</th>
                    <th>Image</th>
                    <th>Content</th>
                    <th>Date</th>
                    <th>Delete</th>
                </tr>
                <?php foreach ($newsList as $newsItem): ?>
                    <tr>
                        <td><?= $newsItem['id']; ?></td>
                        <td><?= $newsItem['title']; ?></td>
                        <td>
                            <?php $image_path = News::getImage($newsItem['id'], true); ?>
                            <img src="<?= $image_path ?>" class="admin_news_img" data-preview-image="<?= $image_path ?>" />
                        </td>
                        <td>
                            <article style="word-break:break-all;"><?= $newsItem['content']; ?></article>
                        </td>
                        <td><?= $newsItem['date']; ?></td>  
                        <td><a href="/cabinet/news/delete/<?= $newsItem['id']; ?>" title="Delete"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
