<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet/">Admin panel</a></li>
                    <li><a href="/cabinet/news/">News management</a></li>
                    <li class="active">Create news</li>
                </ol>
            </div>
            <form method="post" enctype="multipart/form-data" id="feedbackForm">
            <h3>Make new post:</h3>
            <?php if(isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?=$error;?></li>
                        <?php endforeach; ?>
                    </ul>
            <?php endif; ?>
            <div class="form-group">
                <label for="title">Title</label>
                <input type="text" name="title" class="form-control" id="newsTitle" placeholder="Enter post title" value="<?= $title ?>" required="true" minlength="<?= $title_min_limit ?>" maxlength="<?= $title_max_limit ?>" />
            </div>
            <div class="form-group">
                <label for="content">Content:</label>
                <textarea class="form-control" name="content" rows="5" id="newsContent" required="true" minlength="<?= $content_min_limit ?>" maxlength="<?= $content_max_limit ?>"><?= $content ?></textarea>
            </div>
            <input type="file" name="image" id="imageInput" onchange="checkFileSize('imageInput')">
            <div class="modal-footer">
                <button class="btn btn-success" type="submit" name="submit">Send feedback</button>
            </div>
        </form>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>