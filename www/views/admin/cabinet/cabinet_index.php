<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <h1>Admin panel:</h1>
            <h3>Welcome, <?=$adminData["login"]?></h3>
            <ul>
                <li><a href="/cabinet/news/">News management</a></li>
                <li><a href="/cabinet/reviews/">Reviews management</a></li>
            </ul>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
