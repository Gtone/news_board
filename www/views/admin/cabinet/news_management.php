<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet/">Admin panel</a></li>
                    <li class="active">News management</li>
                </ol>
            </div>
            <ul>
                <li><a href="/cabinet/news/list">Show list</a></li>
                <li><a href="/cabinet/news/create">Create news</a></li>
            </ul>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
