<?php include ROOT . '/views/layouts/header.php'; ?>

<section>
    <div class="container">
        <div class="row">
            <div class="breadcrumbs">
                <ol class="breadcrumb">
                    <li><a href="/cabinet/">Admin panel</a></li>
                    <li class="active">List of reviews</li>
                </ol>
            </div>
            <h3>The list of reviews:</h3>
            
            <table class="table-bordered table-striped table">
                <tr>
                    <th>ID</th>
                    <th>Name</th>
                    <th>Email</th>
                    <th>Text</th>
                    <th>Date</th>
                    <th>Update timestamp</th>
                    <th>Update</th>
                    <th>Delete</th>
                </tr>
                <?php foreach ($reviewsList as $reviewItem): ?>
                    <tr>
                        <td><?= $reviewItem['id']; ?></td>
                        <td><?= $reviewItem['name']; ?></td>
                        <td><?= $reviewItem['email']; ?></td>
                        <td>
                            <article style="word-break:break-all;"><?= $reviewItem['text']; ?></article>
                        </td>  
                        <td><?= $reviewItem['date']; ?></td>  
                        <td><?= $reviewItem['update_timestamp']; ?></td>
                        <td><a href="/cabinet/reviews/edit/<?= $reviewItem['id']; ?>" title="Update"><i class="fa fa-pencil-square-o"></i></a></td>
                        <td><a href="/cabinet/reviews/delete/<?= $reviewItem['id']; ?>" title="Delete"><i class="fa fa-times"></i></a></td>
                    </tr>
                <?php endforeach; ?>
            </table>
        </div>
    </div>
</section>

<?php include ROOT . '/views/layouts/footer.php'; ?>
