<?php include ROOT . '/views/layouts/header.php'; ?>

<form method="post">
    <h2><?= "Edit post with id = " . $reviewItemData["id"] ?></h2>
    <?php if(isset($errors) && is_array($errors)): ?>
        <ul>
            <?php foreach ($errors as $error): ?>
                <li><?=$error;?></li>
            <?php endforeach; ?>
        </ul>
    <?php endif; ?>
    <div class="form-group">
        <label for="name">Author's name:</label>
        <input type="text" name="name" class="form-control" required="true" minlength="<?= $name_min_limit ?>" maxlength="<?= $name_max_limit ?>" value='<?=$reviewItemData["name"]?>' />
    </div>
    <div class="form-group">
        <label for="email">Email:</label>
        <input type="email" name="email" class="form-control" required="true" minlength="<?= $email_min_limit ?>" maxlength="<?= $email_max_limit ?>" value='<?=$reviewItemData["email"]?>'>
    </div>
    <div class="form-group">
        <label for="text">Post content:</label>
        <textarea class="form-control" rows="5" required="true" minlength="<?= $text_min_limit ?>" maxlength="<?= $text_max_limit ?>" name="text"><?=$reviewItemData["text"]?></textarea>
    </div>
    <div class="form-group">
        <label>Create timestamp:</label>
        <label name='create_timestamp'><?=$reviewItemData["date"]?></label>
        <br>
        <label>Update timestamp:</label>
        <label name='create_timestamp'><?=$reviewItemData["update_timestamp"]?></label>
    </div>    
    <div class="modal-footer">
        <button class="btn btn-success" type="submit" name="update">Update</button>
        <!--<button class="btn btn-default" type="submit" name="back">Update</button>-->
        <a class="btn btn-default" href="/cabinet/reviews/list/" name="go_back">Go back</a>
    </div>
</form>

<?php include ROOT . '/views/layouts/footer.php'; ?>