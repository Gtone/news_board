<?php include (ROOT . '/views/layouts/header.php'); ?>
<div class="col-md-10 col-sm-6 col-md-offset-1 text-center">
    <h2>Recent news:</h2>
</div>
<?php foreach ($newsList as $newsItem): ?>
<div class="wrapper container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2><a href="/news/<?=$newsItem['id']?>"><?=$newsItem['title']?></a></h2>
            <article style="word-break:break-all;"><?=$newsItem['content']?></article>
            <p>
                <?php if(News::imageExists($newsItem['id'])):?>
                    <img src="<?php echo News::getImage($newsItem['id'], false); ?>" width="200" alt="" />
                <?php endif;?>
            </p>
            <span class="glyphicon glyphicon-time"></span>&nbsp;<i title="<?=$newsItem['date']?>"><?=$newsItem['date_format']?></i>
        </div>
    </div>
</div>
<?php endforeach; ?>
<div class="col-sm4 col-sm-offset-5">
<!-- Pagination -->
<?php echo $pagination->get(); ?>
</div>

<?php include (ROOT . '/views/layouts/footer.php'); ?>