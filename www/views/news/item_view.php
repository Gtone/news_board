<?php include (ROOT . '/views/layouts/header.php'); ?>

<div class="wrapper container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2">
            <h2><?=$newsItem['title']?></h2>
            <p style="word-break:break-all;"><?=$newsItem['content']?></p>
            <p>
                <?php if(News::imageExists($newsItem['id'])):?>
                    <img src="<?php echo News::getImage($newsItem['id'], false); ?>" alt="" />
                <?php endif;?>
            </p>
            <span class="glyphicon glyphicon-time"></span>&nbsp;<i title="<?=$newsItem['date']?>"><?=$newsItem['date_format']?></i>
            <br>
            <button class="btn btn-default" onclick="goBack()">Go Back</button>

            <script>
                function goBack() {
                  window.history.back();
                }
            </script>
        </div>
    </div>
</div>

<?php include (ROOT . '/views/layouts/footer.php'); ?>