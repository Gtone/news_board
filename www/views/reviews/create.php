<?php include (ROOT . '/views/layouts/header.php'); ?>

<div class="container">
    <div class="col-md-10 col-sm-6 col-md-offset-1">
            <form method="post">
                <div class="text-center">
                    <h4>What is on your mind?</h4>
                </div>
                <?php if(isset($errors) && is_array($errors)): ?>
                    <ul>
                        <?php foreach ($errors as $error): ?>
                            <li><?=$error;?></li>
                        <?php endforeach; ?>
                    </ul>
                <?php endif; ?>
                <div class="well">
                    <div class="form-group">
                        <label for="name">Name</label>
                        <input type="text" name="name" class="form-control"
                               pattern="^[a-zA-Z0-9]+$"
                               title="Имя может содержать только цифры и буквы"
                               id="userName" placeholder="Введите ваше имя" 
                               required="true" 
                               minlength="<?= $name_min_limit ?>" 
                               maxlength="<?= $name_max_limit ?>" 
                               value='<?= $name ?>' 
                        />
                    </div>
                    <div class="form-group">
                        <label for="email">Email</label>
                        <input type="email" name="email" class="form-control" 
                               id="email" 
                               placeholder="Введите email"
                               required="true" 
                               minlength="<?= $email_min_limit ?>" 
                               maxlength="<?= $email_max_limit ?>" 
                               value='<?= $email ?>'
                        />
                    </div>
                    <textarea class="form-control" name="text" rows="5" 
                              id="comment" 
                              required="true" 
                              minlength="<?= $text_min_limit ?>" 
                              maxlength="<?= $text_max_limit ?>" 
                    ><?= $text ?></textarea> 
                    <br />
                    <div role="captcha">
                        <label for="captcha_code">Captcha:</label>
                        </br>
                        <?php
                            $_SESSION['captcha'] = simple_php_captcha();
                            echo '<img src="' . $_SESSION['captcha']['image_src'] . '" alt="CAPTCHA" />';
                        ?>
                        </br>
                        <div class="captcha_code_text">
                            <input type="text" name="captcha_code" value="" class="form-control" required="true" />
                        </div>
                    </div>
                    </br>
                    <button class="btn btn-primary btn-sm" type="submit" name="submit"><span class="glyphicon glyphicon-comment"></span> Add Comment</button>
                </div>
            </form>
    </div>
</div>

<?php include (ROOT . '/views/layouts/footer.php'); ?>