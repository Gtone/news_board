<?php include (ROOT . '/views/layouts/header.php'); ?>

<div class="container">
    <div class="col-md-10 col-sm-6 col-md-offset-1 text-center">
        <h2>Recent comments:</h2>
        
        <div class="well">
            <ul id="sortable" class="list-unstyled ui-sortable">
            <?php foreach ($reviewsList as $reviewItem): ?>
                <li class="ui-state-default">
                    </br>
                    <strong class="pull-left primary-font"><?=$reviewItem['name']?></strong> 
                    </br>
                    <strong class="pull-left primary-font"><?=$reviewItem['email']?></strong> 
                    </br>
                    <article style="word-break:break-all;"><?=$reviewItem['text']?></article>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time"></span>&nbsp;<span title="<?=$reviewItem['date']?>"><?=$reviewItem['date_format']?></span>
                    </small> 
                    </br>
                    <?php if ($reviewItem['update_timestamp'] != '0000-00-00 00:00:00'): ?>
                    <small class="pull-right text-muted">
                        <span class="glyphicon glyphicon-time"></span>&nbsp;Changed by admin <span title="<?=$reviewItem['update_timestamp']?>"><?=$reviewItem['update_timestamp_format']?></span>
                    </small>
                    </br>
                    <?php endif; ?>
                </li>
            <?php endforeach; ?>
            </ul>
        </div>
        <div class="col-md-10 col-sm-6 col-md-offset-1">
            <!-- Pagination -->
            <?php echo $pagination->get(); ?>
        </div>
        <div class="col-md-10 col-sm-6 col-md-offset-1">
            <a href="/reviews/add" class="btn btn-primary btn-sm"><span class="glyphicon glyphicon-comment"></span> Оставить свой отзыв</a>
        </div>
    </div>
</div>

<?php include (ROOT . '/views/layouts/footer.php'); ?>