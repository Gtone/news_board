<?php
/**
 * Admin panel controller class
 */
class CabinetController {
    /**
     * Render admin panel template
     * @return boolean
     */
    public function actionIndex() {
        require_once (ROOT . '/views/admin/cabinet/cabinet_index.php');
        
        return true;
    }
    
    /**
     * Render news management template
     * @return boolean
     */
    public function actionNews() {
        require_once (ROOT . '/views/admin/cabinet/news_management.php');
        
        return true;
    }
    
    /**
     * Render news list
     * @return boolean
     */
    public function actionNewsShow() {
        $newsList = News::getNewsList();
        require_once (ROOT . '/views/admin/cabinet/news/show_list.php');
        
        return true;
    }
    
    /**
     * If image exists - check it for restriction, otherwise return false
     * @param array $errors Array with list of errros for displaying on the page
     * @return string Extension of uploaded file or empty string if file is not detected
     */
    private function checkImage(&$errors) {
        // 1. If file is not uploaded - return
        if($_FILES["image"]["error"] == UPLOAD_ERR_NO_FILE) {
            return "";
        }
        
        // 2. Check POST max size or other errors
        if($_FILES["image"]["error"] != UPLOAD_ERR_OK){
            $errors[] = 'Wrong file';
        }
        
        // 3. If file is uploaded properly
        $imgExt = "";
        if (is_uploaded_file($_FILES["image"]["tmp_name"])) {
            // 4. Check file extension
            $fileExtension = pathinfo($_FILES["image"]["name"], PATHINFO_EXTENSION);
            $imgExt = $fileExtension;
            $allowedImageExtension = array(
                "png",
                "jpg",
                "jpeg",
                "gif"
            );
            if (!in_array($fileExtension, $allowedImageExtension)) {
                $errors[] = "<strong>$fileExtension</strong> - Wrong image extension (PNG, JPG, JPEG, GIF are allowed)";
            }
            
            // 5. Check max image size
            $maxSize = 2097152;
            if (($_FILES["image"]["size"] > $maxSize)) {
                $maxSizeMb = $maxSize / 1024 / 1024;
                $errors[] = 'Image size exceeds ' . $maxSizeMb . 'MB';
            }
            
            // 6. Check image file dimension
            $fileinfo = @getimagesize($_FILES["image"]["tmp_name"]);
            $width = $fileinfo[0];
            $maxImageWidth = 800;
            $height = $fileinfo[1];
            $maxImageHeight = 600;
            if ($width > $maxImageWidth || $height > $maxImageHeight) {
                $errors[] = 'Image dimension should be within ' . $maxImageWidth . 'x' . $maxImageHeight;
            }
        }
        
        // 7. Return image extension
        return $imgExt;
    }
    
    /**
     * If file uploaded, builds path to image 
     * and create file on server
     * @param int $id
     * @param string $ext
     * @return boolean
     */
    private function createImage($id, $ext) {
        if (!is_uploaded_file($_FILES["image"]["tmp_name"])) {
            return false;
        }
        
        move_uploaded_file($_FILES["image"]["tmp_name"], $_SERVER["DOCUMENT_ROOT"] . "/upload/images/$id.$ext");
        return true;
    }
    
    /**
     * Creates news post
     * @return boolean
     */
    public function actionNewsCreate() {
        $title = "";
        $content = "";
        
        $title_min_limit = 4;
        $title_max_limit = 50;

        $content_min_limit = 10;
        $content_max_limit = 4096;
        
        if (isset($_POST['submit'])) {
            //Getting data from form
            $title = htmlspecialchars(trim($_POST['title']));
            $content = htmlspecialchars(trim($_POST['content']));
            
            //Errors flag
            $errors = false;
            
            // Check title length
            if (!News::countParam($title, $title_min_limit, $title_max_limit)) {
                $errors[] = 'Title must be at least ' . $title_min_limit . ' characters and not more then ' . $title_max_limit . ' symbols!';
            }
            
            // Check content length
            if (!News::countParam($content, $content_min_limit, $content_max_limit)) {
                $errors[] = 'Content must be at least ' . $content_min_limit . ' characters ' . $content_max_limit . '!';
            }
            
            // Process image uploading
            $imageExt = $this->checkImage($errors);
            
            if ($errors == false) {
                //Getting id, that returned after success news create
                $id = News::createNews($title, $content, $imageExt);
                
                //Green alert post sucseed
                $isImageCreated = true;
                if ($id && $imageExt) {
                    $isImageCreated = $this->createImage($id, $imageExt);
                }
                
                if ($isImageCreated) {
                    $title = "";
                    $content = "";
                    require_once (ROOT . '/views/admin/cabinet/news/create_success.php');
                } else {
                    $errors[] = "Cant' save image";
                }
            }
        }
        
        require_once (ROOT . '/views/admin/cabinet/news/create.php');
        return true;
    }
    
    /**
     * Delete news item
     * @param int $id
     * @return boolean
     */
    public function actionNewsDelete ($id) {
        //check for wrong news id
        if (News::checkNewsId($id)) {
            $imageName = News::imageExists($id);
            if ($imageName) {
                unlink(ROOT . $imageName);
            }
            News::deleteNewsById($id);
            header('Location: /cabinet/news/list');
        } else {
            require_once (ROOT . '/views/layouts/404.php');
        }
        
        return true;
    }
    
    /**
     * Redirects to reviews management table
     * @return boolean
     */
    public function actionReviews() {
        header('Location: /cabinet/reviews/list');
        
        return true;
    }
    
    /**
     * Delete review
     * @param int $id
     * @return boolean
     */
    public function actionReviewsDelete($id) {
        //check for wrong news id
        if (Reviews::checkReviewId($id)) {
            Reviews::deleteReviewById($id);
            header('Location: /cabinet/reviews/list');
        } else {
            require_once (ROOT . '/views/layouts/404.php');
        }
        
        return true;
    }
    
    /**
     * Edit existing review
     * @param int $id
     */
    public function actionReviewsEdit($id) {
        if (Reviews::checkReviewId($id)) {
            $name = "";
            $name_min_limit = 4;
            $name_max_limit = 50;
            
            $email = "";
            $email_min_limit = 4;
            $email_max_limit = 100;
            
            $text = "";
            $text_min_limit = 10;
            $text_max_limit = 4096;
            
            if (isset($_POST['update'])) {
                $name = htmlspecialchars(trim($_POST['name']));
                $email = trim($_POST['email']);
                $text = htmlspecialchars(trim($_POST['text']));

                //Errors flag
                $errors = false;

                // Check for length
                if(!Reviews::countParam($name, $name_min_limit, $name_max_limit)) {
                    $errors[] = "Your name must be at least $name_min_limit and not more then $name_max_limit!";
                }
                if(!Reviews::countParam($text, $text_min_limit, $text_max_limit)) {
                    $errors[] = "Your comment must be at least $text_min_limit characters and not more then $text_max_limit!";
                }
                if(!Reviews::countParam($email, $email_min_limit, $email_max_limit)) {
                    $errors[] = "Your email must be at least $email_min_limit characters and not more then $email_max_limit!";
                }
                
                // Check for valid email
                if(!Reviews::checkEmail($email)) {
                    $errors[] = "Неправильный email!";
                }
                
                // Check for valid name
                if(!Reviews::checkName($name)) {
                    $errors[] = "Имя может содержать только цифры или буквы!";
                }

                if ($errors == false) {
                    $result = Reviews::updateReview($id, $name, $email, $text);
                    //header('Location: /cabinet/reviews/list');
                    if ($result) {
                        //redirect /cabinet/reviews
                        $name = "";
                        $email = "";
                        $text = "";
                        require_once (ROOT . '/views/admin/cabinet/news/create_success.php');
                    } else {
                        print "Update error";
                    }
                    
                }
            }
            $reviewItemData = Reviews::getReviewById($id);
            require_once (ROOT . '/views/admin/cabinet/reviews/edit.php');
        } else {
            require_once (ROOT . '/views/layouts/404.php');
        }
    }
    
    /**
     * Shows list of reviews, render show_list template
     * @return boolean
     */
    public function actionReviewsShow() {
        $reviewsList = Reviews::getReviewsList();
        require_once (ROOT . '/views/admin/cabinet/reviews/show_list.php');
        
        return true;
    }
}