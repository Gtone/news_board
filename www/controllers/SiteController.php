<?php
/**
 * Site controller class
 * can be customized for main page actions
 */
class SiteController {
    /**
     * Redirects to news list view
     */
    public function actionIndex(){
        header('Location: /news/');
    }
}
