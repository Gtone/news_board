<?php
/**
 * News controller class
 * actions for users
 */
class NewsController {
    /**
     * Renders news list, with pagination
     * @param int $page
     * @return boolean
     */
    public function actionIndex($page = 1) {
        $newsList = array();
        $newsList = News::getNewsLimit($page);
        
        $total = News::getNewsCount();
        //Exec paginator
        $pagination = new Pagination($total, $page, News::SHOW_BY_DEFAULT, 'page-');
        
        require_once(ROOT . '/views/news/list_view.php');
        
        return true;
    }
    
    /**
     * Render one news post
     * @param type $id
     * @return boolean
     */
    public function actionView($id) {
        $checkId = News::checkNewsId($id);
        //Redirect to 404, if news doesn't exists
        if(!$checkId) {
             require_once (ROOT . '/views/layouts/404.php');
        } else {
            $newsItem = array();
            $newsItem = News::getNewsById($id);
            require_once(ROOT . '/views/news/item_view.php');
        }
        return true;
    }
}
