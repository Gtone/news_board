<?php
/**
 * Reviews controller class
 * actions for users
 */
class ReviewsController {
    /**
     * Renders reviews list, with pagination
     * @param int $page
     * @return boolean
     */
    public function actionIndex($page = 1) {
        $reviewsList = array();
        $reviewsList = Reviews::getReviewsLimit($page);
        
        $total = Reviews::getReviewsCount();
        
        $pagination = new Pagination($total, $page, Reviews::SHOW_BY_DEFAULT, 'page-');
        
        require_once(ROOT . '/views/reviews/list_view.php');
        
        return true;
    }
    
    /**
     * Render review creation form
     * adds new review
     * @return boolean
     */
    public function actionAdd() {
        $name = "";
        $name_min_limit = 4;
        $name_max_limit = 50;

        $email = "";
        $email_min_limit = 4;
        $email_max_limit = 100;

        $text = "";
        $text_min_limit = 10;
        $text_max_limit = 4096;

        if (isset($_POST['submit'])) {
            $name = htmlspecialchars(trim($_POST['name']));
            $email = trim($_POST['email']);
            $text = htmlspecialchars(trim($_POST['text']));
            $captcha = ($_POST['captcha_code']);
            $captcha_required = ($_SESSION['captcha']['code']);
            
            //Errors flag
            $errors = false;

            // Check for captcha
            if(strcmp($captcha, $captcha_required)) {
                $errors[] = "You've entered incorrect CAPTCHA. ";
            } else {
                // Check for length
                if(!Reviews::countParam($name, $name_min_limit, $name_max_limit)) {
                    $errors[] = "Your name must be at least $name_min_limit and not more then $name_max_limit!";
                }
                if(!Reviews::countParam($text, $text_min_limit, $text_max_limit)) {
                    $errors[] = "Your comment must be at least $text_min_limit characters and not more then $text_max_limit!";
                }
                if(!Reviews::countParam($email, $email_min_limit, $email_max_limit)) {
                    $errors[] = "Your email must be at least $email_min_limit characters and not more then $email_max_limit!";
                }

                // Check for valid email
                if(!Reviews::checkEmail($email)) {
                    $errors[] = "Неправильный email!";
                }

                // Check for valid name
                if(!Reviews::checkName($name)) {
                    $errors[] = "Имя может содержать только цифры или буквы!";
                }
            }
            
            if ($errors == false) {
                $id = Reviews::createReview($name, $email, $text);
                header('Location: /reviews/');
            }
        } 
        require_once (ROOT . '/views/reviews/create.php');
        return true;
    }
}
