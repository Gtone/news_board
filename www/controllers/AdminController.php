<?php
/**
 * Admin controller class, only check login or guest
 * 
 */
class AdminController {
    /**
     * Login to admin panel method
     * @return boolean
     */
    public function actionLogin() {        
        $login = "";
        $password = "";
    if (isset($_POST['submit'])) {
        //Getting data from post
            $login = $_POST['login'];
            $password = $_POST['password'];
            //Errors flag - default false
            $errors = false;
            //Check login length
            if(!Admin::countName($login)) {
                $errors[] = "Login is too short";
            }
            //Check password length
            if(!Admin::countPassword($password)) {
                $errors[] = "Password must be atleast 6 characters!";
            }
            //Get admin data fro db by login
            $adminData = Admin::getAdminByLogin($login);
            //Check for admin existence
            if ($adminData != NULL) {
                $check = Admin::checkPwd($password, $adminData);
                if ($check) {
                        Admin::auth($adminData['id']);
                        //Redirect to admin panel here
                        header('Location: /cabinet/');
                    } else {
                        $errors[] = "Wrong password";
                    }
            } else {
                //If admin data null, there is no record in database
                 $errors[] = "Wrong login data";
            }
        }
        //If we not logged in - render admin login page
        if (Admin::isGuest()){
            require_once(ROOT . '/views/admin/admin_index.php');
        } else {
            //Render admin panel if we logged in
            header('Location: /cabinet/');
        }
        return true;
    }
    
    /**
     * Removes admin id from session
     */
    public function actionLogout () {
        unset($_SESSION["admin"]);
        header("Location: / ");
    }
    
}
