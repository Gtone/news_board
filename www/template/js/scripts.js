// make readmore
$('article').readmore({
    maxHeight: 240,
    moreLink: '<a href="#">Подробнее</a>',
    lessLink: '<a href="#">Скрыть</a>'
});

// make scroll
$("#scrollToTop").click(function() {
     $("html, body").animate({ scrollTop: 0 }, "fast");
     return false;
});
window.onscroll = function() {scrollFunction()};
function scrollFunction() {
  if (document.body.scrollTop > 300 || document.documentElement.scrollTop > 300) {
    document.getElementById("scrollToTop").style.display = "block";
  } else {
    document.getElementById("scrollToTop").style.display = "none";
  }
}

// check file size
function checkFileSize(elementId, maxFileSize = 2097152) {
    if((document.getElementById(elementId).files.length) <= 0){
        return;
    }
    var sizef = document.getElementById(elementId).files[0].size;
    if(sizef > maxFileSize){
        var maxFileSizeM = maxFileSize / 1024 / 1024;
        document.getElementById(elementId).value = "";
        alert('File size exceded of maximum allowed size ' + maxFileSizeM + 'mb');
    }
}