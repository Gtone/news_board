<?php

/**
 * Класс Router
 * Компонент для работы с маршрутами
 */
class Router
{

    /**
     * Свойство для хранения массива роутов
     * @var array 
     */
    private $routes;
    
    /** 
     * Array flags for admin permission
     * @var array
     */
    private $permissions;

    /**
     * Конструктор
     */
    public function __construct()
    {
        // Путь к файлу с роутами
        $routesPath = ROOT . '/config/routes.php';
        $permissionsPath = ROOT . '/config/permissions.php';
        // Получаем роуты из файла
        $this->routes = include($routesPath);
        $this->permissions = include($permissionsPath);
    }

    /**
     * Возвращает строку запроса
     */
    private function getURI()
    {
        if (!empty($_SERVER['REQUEST_URI'])) {
            return trim($_SERVER['REQUEST_URI'], '/');
        }
    }

    /**
     * Метод для обработки запроса
     */
    public function run()
    {
        // Получаем строку запроса
        $uri = $this->getURI();
        $isControllerDetected = false;
        // Проверяем наличие такого запроса в массиве маршрутов (routes.php)
        foreach ($this->routes as $uriPattern => $path) {
            // Сравниваем $uriPattern и $uri
            if(preg_match("~^$uriPattern$~", $uri)) {
                
                // Получаем внутренний путь из внешнего согласно правилу.
                $internalRoute = preg_replace("~^$uriPattern$~", $path, $uri);

                // Определить контроллер, action, параметры

                $segments = explode('/', $internalRoute);
                
                $controllerName = array_shift($segments) . 'Controller';
                $controllerName = ucfirst($controllerName);
                
                $actionName = 'action' . ucfirst(array_shift($segments));

                $parameters = $segments;
                
                // Подключить файл класса-контроллера
                /*$controllerFile = ROOT . '/controllers/' .
                        $controllerName . '.php';

                if (file_exists($controllerFile)) {
                    include_once($controllerFile);
                }*/

                // Создать объект, вызвать метод (т.е. action)
                $controllerObject = new $controllerName;

                /* Вызываем необходимый метод ($actionName) у определенного 
                 * класса ($controllerObject) с заданными ($parameters) параметрами
                 */
                
                //is auth required
                $keyName = $controllerName . '/' . $actionName;
                //print $keyName;
                $keyExists = key_exists($keyName, $this->permissions);
                if ($keyExists && $this->permissions[$keyName] == true) {
                    //Admin must be authorized
                    Admin::checkLogged();
                    //return;
                }
                define('CONTROLLER_NAME', $controllerName);
                $result = call_user_func_array(array($controllerObject, $actionName), $parameters);
                
                $isControllerDetected = true;
                // Если метод контроллера успешно вызван, завершаем работу роутера
                if ($result != null) {
                    break;
                }
            }
        }
        //Render default controller if we can't find match from routes
        if (!$isControllerDetected) {
            $controllerObject = new DefaultController();
            $controllerObject->default404();
        }
        if (!defined('CONTROLLER_NAME')){
            define('CONTROLLER_NAME', "");
        }
        
    }

}
