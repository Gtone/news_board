<?php
/**
 * Base model for ::self issues
 */
    class Base {
        /**
         * Get current time value, using MySQL NOW 
         * @return datbase row
         */
        protected static function getCurrentTime() {
            $db = Db::getConnection();
            
            $result = $db->query('SELECT NOW() as time;');
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $data = $result->fetch();
            return $data['time'];
        }
        /**
         * Calculates current time values with 
         * date from database
         * @param db row $since
         * @return string
         */
        protected static function getTimeSince($since) {
            $currentTimestamp = self::getCurrentTime();
            $timestamp = self::dateToTimestamp($since); 
            $datetime1 = new DateTime($currentTimestamp);
            $datetime2 = new DateTime($since);
            $diff = date_diff($datetime1, $datetime2);
            
            $timemsg = '';
            if($diff->y > 0){
                $timemsg .= $diff->y .' year'. ($diff->y > 1?"'s":'') . ' ';
            }
            if($diff->m > 0){
                $timemsg .= $diff->m . ' month'. ($diff->m > 1?"'s":'') . ' ';
            }
            if($diff->d > 0){
                $timemsg .= $diff->d .' day'. ($diff->d > 1?"'s":'') . ' ';
            } 
            if($diff->h > 0){
                $timemsg .= $diff->h .' hour'.($diff->h > 1 ? "'s":'') . ' ';
            }
            if($diff->i > 0){
                $timemsg .= $diff->i .' minute'. ($diff->i > 1?"'s":'') . ' ';
            }
            if (!$timemsg) {
                $timemsg = 'just now';
            } else {
                $timemsg .= 'ago';
            }
            
            return $timemsg;
        }
        
        /**
         * Convert date to int
         * @param date $date
         * @return int
         */
        private static function dateToTimestamp($date) {
            if ($date == 0) {
                return 0;
            }
            $date_parsed = date_parse_from_format('Y-m-d H:i:s', $date);
            return mktime($date_parsed['hour'], $date_parsed['minute'], $date_parsed['second'], $date_parsed['month'], $date_parsed['day'], $date_parsed['year']);
        }
        
    }
