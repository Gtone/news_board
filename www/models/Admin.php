<?php
class Admin extends Base {
    /**
     * Checks record existing in database. Returns false if no record in database
     * @param int $id
     * @return boolean
     */
    public static function checkAdminId ($id) {
        $db = Db::getConnection();
        $sql = ("SELECT * FROM admin WHERE id = $id;");
        $result = $db->prepare($sql);
        $result->execute();
        $count = $result->rowCount();
        //Checking for 0 rows,
        // when rows in record is 0, there is no this kind of record
        if ($count === 0) {
            return false;
        } else {
            return true;
        }
    }
    /**
     * Set adminId to session
     * @param string $adminId
     */
    public static function auth($adminId) {
        $_SESSION['admin'] = $adminId;
    }
    
    /**
     * Checking for admin id in session. Returns redirect to admin panel login, if not logged.
     * @return string | header action(no returned value)
     */
    public static function checkLogged () {
        if(isset($_SESSION['admin'])){
            return $_SESSION['admin'];
        }
        
        header ("Location: /admin/");
    }
    
    /**
     * Checks for guest, returns true if you're guest 
     * @return boolean
     */
    public static function isGuest() {
        if(isset($_SESSION['admin'])) {
            return false;
        }
        return true;
    }
    
    /**
     * Return record from admin table by id
     * @param int $id
     * @return PDO fetch result
     */
    public static function getAdminById($id) {
        if($id) {
            $db = Db::getConnection();
            $sql = 'SELECT * FROM admin WHERE id = :id;';
            
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            
            $result->setFetchMode(PDO::FETCH_ASSOC);
            $result->execute();
            
            return $result->fetch();
        }
    }
    
    /**
     * Compare string password from params, with password from database data
     * Passwords in database stores as md5 hash,
     * so string password from param assigned to md5
     * @param string $password
     * @param array $adminData
     * @return boolean
     */
    public static function checkPwd ($password, $adminData) {
        //Hash md5 password, that we compare
        $md5Password = md5($password);
        $check = strcmp($adminData["password"], $md5Password);
        return ($check == 0);
    }

    /**
     * Select admin record from database, by login from param
     * @param type $login
     * @return database record (fetch result) or null
     */
    public static function getAdminByLogin ($login) {
        $db = Db::getConnection();
        
        $sql = "SELECT * FROM admin WHERE login = '$login';";
        $result = $db->prepare($sql);
        $result->execute();
        //Return null if record empty
        if ($result === false) {
            return null;
        }
        //Fetch db record if exec success
        $row = $result->fetch();
        
        return $row;
    }
    
    /**
     * Count password from param
     * @param string $password
     * @return boolean
     */
    public static function countPassword($password) {
        if (strlen($password) >=6) {
            return true;
        }
        return false;
    }
    
    /**
     * Count name from param
     * @param string $name
     * @return boolean
     */
    public static function countName($name) {
        if (strlen($name) >=4) {
            return true;
        }
        return false;
    }
}
