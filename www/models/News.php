<?php
/**
 * News model class
 */
class News extends Base {
    //The number of news displayed by default
    const SHOW_BY_DEFAULT = 3;
    //Path to news images
    const IMAGES_PATH = '/upload/images/';
    
    /**
     * Returns one news from database
     * @param int $id
     * @return db row
     */
    public static function getNewsById($id) {
        $id = intval($id);
        
        if ($id) {
            $db = Db::getConnection();
            
            $result = $db->query('SELECT * FROM news WHERE id =' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            $newsItem = $result->fetch();
            //Calculate time for "post ago"
            $newsItem['date_format'] = self::getTimeSince($newsItem['date']);
            return $newsItem;
        }
    }
    
    /**
     * Returns array of all news from database
     * @return array
     */
    public static function getNewsList() {        
        $db = Db::getConnection();
        
        $newsList = array();
        
        $sql = ('SELECT * '
                . 'FROM news '
                . 'ORDER BY id DESC;');
        $result = $db->prepare($sql);
        $result->execute();
        $i = 0;
        while($row = $result->fetch()) {
            $newsList[$i]['id'] = $row['id'];
            $newsList[$i]['title'] = $row['title'];
            $newsList[$i]['content'] = $row['content'];
            $newsList[$i]['date'] = $row['date'];
            //Calculate time for "post ago"
            $newsList[$i]['date_format'] = self::getTimeSince($newsList[$i]['date']);
            $newsList[$i]['image_ext'] = $row['image_ext'];
            $i++;
        }
        
        return $newsList;
    }
    
    /**
     * Returns list of news with offset for pagination
     * @param int $page
     * @return array
     */
    public static function getNewsLimit($page=1) {
        //Set entrys per one page
        $limit = self::SHOW_BY_DEFAULT;
        //Calculate offset for db
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        
        $db = Db::getConnection();
        
        $sql = 'SELECT * FROM news ORDER BY date DESC LIMIT :limit OFFSET :offset ';
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->execute();
        $i = 0;
        $news = array();
        while ($row = $result->fetch()) {
            $news[$i]['id'] = $row['id'];
            $news[$i]['title'] = $row['title'];
            $news[$i]['content'] = $row['content'];
            $news[$i]['date'] = $row['date'];
            $news[$i]['date_format'] = self::getTimeSince($news[$i]['date']);
            $news[$i]['image_ext'] = $row['image_ext'];
            $i++;
        }
        return $news;
    }
    
    /**
     * Returns number of news records from db
     * counts id's
     * @return string
     */
    public static function getNewsCount() {
        $db = Db::getConnection();
        
        $result = $db->query("SELECT count(id) AS count FROM news;");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        
        return $row['count'];
    }
    
    /**
     * Check news existence
     * @param int $id
     * @return boolean
     */
    public static function checkNewsId ($id) {
        $db = Db::getConnection();
        $sql = ("SELECT * FROM news WHERE id = $id;");
        $result = $db->prepare($sql);
        $result->execute();
        $count = $result->rowCount();
        //Checking for 0 rows,
        // when rows in record is 0, there is no this kind of record
        if ($count === 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Count string length and returns false if $limit exceeded
     * @param string $param
     * @param int $min_limit
     * @param int $max_limit
     * @return boolean
     */
    public static function countParam($param, $min_limit, $max_limit = 0) {
        // check for min limit
        if (strlen($param) < $min_limit) {
            return false;
        }
        
        // check for max limit
        if ($max_limit != 0 && strlen($param) > $max_limit) {
            return false;
        }
        
        // all checks are passed
        return true;
    }
    
    /**
     * Create new db record for news
     * returns zero if query failed or just created record id
     * @param string $title
     * @param string $content
     * @param string $imageExt
     * @return int
     */
    public static function createNews($title, $content, $imageExt = "") {
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO news (title, content, image_ext) '
                . 'VALUES (:title, :content, :image_ext);';
        $result = $db->prepare($sql);
        $result->bindParam(':title', $title, PDO::PARAM_STR);
        $result->bindParam(':content', $content, PDO::PARAM_STR);
        $result->bindParam(':image_ext', $imageExt, PDO::PARAM_STR);
        if ($result->execute()) {
            //If query success, return id of last record
            return $db->lastInsertId();
        }
        // Else return zero
        return 0;
    }
    
    /**
     * Delete news record from db
     * @param int $id
     * @return db query result
     */
    public static function deleteNewsById($id) {
        // Connect to db
        $db = Db::getConnection();

        // SQL query for db
        $sql = 'DELETE FROM news WHERE id = :id';

        // Receieving and return of results, using prepeared query
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    
    /**
     * Check image existence
     * @param type $id
     * @return boolean|string
     */
    public static function imageExists($id) {
        $path = self::IMAGES_PATH;
        $itemData = self::getNewsById($id);
        //Get image extension from database, by id of record
        $image_ext = $itemData["image_ext"];
        //Build path to image
        $pathToNewsImage = $path . $id . ".$image_ext";

        if (file_exists($_SERVER['DOCUMENT_ROOT'].$pathToNewsImage)) {
            // If image exists
            // Return path
            $fakeImage = false;
            return $pathToNewsImage;
        } else {
            return false;
        }
    }
    
    /**
     * Returns image path if it exists
     * If we don't have image and want to use blank image,
     * set $fakeImage - true
     * We can not use fake image, by setting $fakeImage to false
     * @param type $id
     * @param type $fakeImage - fake image flag
     * @return string
     */
    public static function getImage($id, $fakeImage = true)
    {
        //Blank default image name
        $noImage = 'no-image.jpg';
        //Default path to images
        $path = self::IMAGES_PATH;
        //Check for real image for news
        $imageExists = self::imageExists($id);
        //Check for usage of blank default image
        if ($imageExists === false) {
            if ($fakeImage) {
                return $path . $noImage; 
            } else {
                return "";
            }
        } else {
            //Return real image path
            return $imageExists;
        }
        
    }
}
