<?php
/**
 * Reviews model class
 */
class Reviews extends Base {
    //The number of reviews displayed by default
    const SHOW_BY_DEFAULT = 4;
    
    /**
     * Returns one review record from db
     * @param int $id
     * @return array
     */
    public static function getReviewById($id) {
        $id = intval($id);
        
        if ($id) {
            $db = Db::getConnection();
            
            $result = $db->query('SELECT * FROM reviews WHERE id =' . $id);
            $result->setFetchMode(PDO::FETCH_ASSOC);
            
            $reviewsItem = $result->fetch();
            $reviewsItem['date_format'] = self::getTimeSince($reviewsItem['date']);
            return $reviewsItem;
        }
    }
    
    /**
     * Returns all reviews from db
     * @return array
     */
    public static function getReviewsList() {        
        $db = Db::getConnection();
        
        $reveiwsList = array();
        
        $sql = ('SELECT * '
                . 'FROM reviews '
                . 'ORDER BY id DESC;');
        $result = $db->prepare($sql);
        $result->execute();
        $i = 0;
        while($row = $result->fetch()) {
            $reveiwsList[$i]['id'] = $row['id'];
            $reveiwsList[$i]['name'] = $row['name'];
            $reveiwsList[$i]['email'] = $row['email'];
            $reveiwsList[$i]['text'] = $row['text'];
            $reveiwsList[$i]['date'] = $row['date'];
            $reveiwsList[$i]['date_format'] = self::getTimeSince($reveiwsList[$i]['date']);
            $reveiwsList[$i]['update_timestamp'] = $row['update_timestamp'];
            //Check for update_timestamp
            //If it exists - admin changed the record
            if ($reveiwsList[$i]['update_timestamp'] != '0000-00-00 00:00:00') {
                $reveiwsList[$i]['update_timestamp_format'] = self::getTimeSince($reveiwsList[$i]['update_timestamp']);
            }
            $i++;
        }
        
        return $reveiwsList;
    }
    
    /**
     * Returns list of reviews with offset for pagination
     * @param type $page
     * @return array
     */
    public static function getReviewsLimit($page=1){
        //Set entrys per one page
        $limit = self::SHOW_BY_DEFAULT;
        //Calculate offset for db
        $offset = ($page - 1) * self::SHOW_BY_DEFAULT;
        
        $db = Db::getConnection();
        
        $sql = 'SELECT * FROM reviews ORDER BY date DESC LIMIT :limit OFFSET :offset ';
        $result = $db->prepare($sql);
        $result->bindParam(':limit', $limit, PDO::PARAM_INT);
        $result->bindParam(':offset', $offset, PDO::PARAM_INT);
        $result->execute();
        $i = 0;
        $reviews = array();
        while ($row = $result->fetch()) {
            $reviews[$i]['id'] = $row['id'];
            $reviews[$i]['name'] = $row['name'];
            $reviews[$i]['email'] = $row['email'];
            $reviews[$i]['text'] = $row['text'];
            $reviews[$i]['date'] = $row['date'];
            $reviews[$i]['date_format'] = self::getTimeSince($reviews[$i]['date']);
            $reviews[$i]['update_timestamp'] = $row['update_timestamp'];
            //Check for update_timestamp
            //If it exists - admin changed the record
            if ($reviews[$i]['update_timestamp'] != '0000-00-00 00:00:00') {
                $reviews[$i]['update_timestamp_format'] = self::getTimeSince($reviews[$i]['update_timestamp']);
            }
            $i++;
        }
        return $reviews;
    }
    
    /**
     * Returns number of reviews records from db
     * @return string
     */
    public static function getReviewsCount() {
        $db = Db::getConnection();
        
        $result = $db->query("SELECT count(id) AS count FROM reviews;");
        $result->setFetchMode(PDO::FETCH_ASSOC);
        $row = $result->fetch();
        
        return $row['count'];
    }
    
    /**
     * Check reviews existence
     * @param type $id
     * @return boolean
     */
    public static function checkReviewId ($id) {
        $db = Db::getConnection();
        $sql = ("SELECT * FROM reviews WHERE id = $id;");
        $result = $db->prepare($sql);
        $result->execute();
        $count = $result->rowCount();
        //Checking for 0 rows,
        // when rows in record is 0, there is no this kind of record
        if ($count === 0) {
            return false;
        } else {
            return true;
        }
    }
    
    /**
     * Count string length and returns false if $limit exceeded
     * @param string $param
     * @param int $limit
     * @return boolean
     */
    public static function countParam($param, $min_limit, $max_limit = 0) {
        // check for min limit
        if (strlen($param) < $min_limit) {
            return false;
        }
        
        // check for max limit
        if ($max_limit != 0 && strlen($param) > $max_limit) {
            return false;
        }
        
        // all checks are passed
        return true;
    }
    
    /**
     * Returns true if $name is match numbers or/and letters pattern
     * @param string $name
     * @return boolean
     */
    public static function checkName($name) {
        if (preg_match('/^[a-zA-Z0-9]+$/', $name)) {
            return true;
        }
        return false;
    }
    
    /**
     * Create new db record for news
     * returns zero if query failed or just created record id
     * @param string $name
     * @param string $email
     * @param string $text
     * @return int
     */
    public static function createReview($name, $email, $text) {
        
        $db = Db::getConnection();
        
        $sql = 'INSERT INTO reviews (name, email, text) '
                . 'VALUES (:name, :email, :text);';
        $result = $db->prepare($sql);
        $result->bindParam(':name', $name, PDO::PARAM_STR);
        $result->bindParam(':email', $email, PDO::PARAM_STR);
        $result->bindParam(':text', $text, PDO::PARAM_STR);
        if ($result->execute()) {
            //If query success, return id of last record
            return $db->lastInsertId();
        }
        // Else return zero
        return 0;
    }
    
    /**
     * Delete reviews record from db
     * @param type $id
     * @return type
     */
    public static function deleteReviewById($id) {
        // Connect to db
        $db = Db::getConnection();

        // SQL query for db
        $sql = 'DELETE FROM reviews WHERE id = :id';

        // Receieving and return of results, using prepeared query
        $result = $db->prepare($sql);
        $result->bindParam(':id', $id, PDO::PARAM_INT);
        return $result->execute();
    }
    
    /**
     * Changes review record data in database
     * @param int $id
     * @param string $name
     * @param string $email
     * @param string $text
     * @return boolean
     */
    public static function updateReview($id, $name, $email, $text) {
        $timestamp = date ('Y-m-d H:i:s');
        $db = Db::getConnection();
        if (self::checkReviewId($id)) {
            $sql = "UPDATE reviews
            SET 
                name = :name, 
                email = :email, 
                text = :text,
                update_timestamp = :timestamp
            WHERE id = :id";
            $result = $db->prepare($sql);
            $result->bindParam(':id', $id, PDO::PARAM_INT);
            $result->bindParam(':name', $name, PDO::PARAM_STR);
            $result->bindParam(':email', $email, PDO::PARAM_STR);
            $result->bindParam(':text', $text, PDO::PARAM_STR);
            $result->bindParam(':timestamp', $timestamp, PDO::PARAM_STR);
            return $result->execute();
        } else {
            return false;
        }
    }
    
    /**
     * Checks email
     * @param string $email <p>E-mail</p>
     * @return boolean <p>Method execution result</p>
     */
    public static function checkEmail($email)
    {
        //Return false if email validation fails
        if (filter_var($email, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
}


